var express = require('express');
var router = express.Router();
var cache = require('../cache/memory.js');

router.get('/', function(req, res, next) {
  res.render('index', { 
    title: 'OpenAirInterface Portal',
    enb2ue: cache.enb2ue,
    ue2enb: cache.ue2enb,
    grafana_host: cache.grafana_host
  });
});

router.get('/enb2ue', function(req, res, next) {
  var value = cache.enb2ue;
  console.log("get enb2ue: ".value);
  if(value === undefined || value === null){
    res.json({ size: 0 });
  }else{
    res.json({ size: value });
  }
});

router.post('/enb2ue', function(req, res, next) {
  var body = req.body;
  var value = body.val;
  if(value === null){
    value = 0;
  }
  console.log("set enb2ue: ".value);
  cache.enb2ue = value;
  res.sendStatus(200);
});

router.get('/ue2enb', function(req, res, next) {
  var value = cache.ue2enb;
  console.log("get ue2enb: ".value);
  if(value === undefined || value === null){
    res.json({ size: 0 });
  }else{
    res.json({ size: value });
  }
});

router.post('/ue2enb', function(req, res, next) {
  var body = req.body;
  var value = body.val;
  if(value === null){
    value = 0;
  }
  console.log("set ue2enb: ".value);
  cache.ue2enb = value;
  res.sendStatus(200);
});

router.post('/grafana_host', function(req, res, next) {
  var body = req.body;
  var value = body.val;
  if(value === null){
    value = 0;
  }
  console.log("set grafana_host: ".value);
  cache.grafana_host = value;
  res.sendStatus(200);
});


module.exports = router;