# LTE MAC Visualization

A Real-time Controller and Visualizer for OpenAirInterface-RAN

## Prerequisites

1. Install Required Software/Libraries
    - [docker](https://docs.docker.com/engine/install/ubuntu/)
    - [docker-compose](https://docs.docker.com/compose/install/)
    - [nodejs](https://github.com/nodesource/distributions#deb)
    - [graphyte](https://github.com/benhoyt/graphyte)

2. Install OpenAirInterface-RAN and Build

```bash
cd ~/<workdir>
mkdir oai
cd oai
git clone https://gitlab.com/zixuanzhong/openairinterface5g.git .
source oaienv
cd cmake_targets/
./build_oai -I --eNB --UE 
```

> The `-I` flag is only needed for the first building

3. Create a folder for logs

```bash
cd ~/
mkdir logs
```

## Installing

```bash
cd ~/<workdir>
mkdir visualization
cd visualization
git clone https://gitlab.com/zixuanzhong/lte-mac-visualization.git .
```

## Deployment

### Local Deployment

Start:
```bash
cd ~/<workdir>/visualization
./launch.sh 127.0.0.1 80 ~/<workdir>/oai ~/logs
```

Go to `http://<ip-of-vm>` in your browser, you should see the Portal page, replace `localhost` with `<ip-of-vm>` and click on the "refresh" button.

Stop:

```bash
cd ~/<workdir>/visualization
./halt.sh
```

### Remote Deployment: Use the scripts inside each module.