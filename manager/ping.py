import argparse
import os
import requests
import signal
import time
from multiprocessing import Pool

def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)

def get_size(host, port, uri):
    url = 'http://{}:{}/{}'.format(host, port, uri)
    try:
        response = requests.get(url)
        size = 0
        result = response.json()
        size = result['size']
    except:
        print("except")
        return 0
    print('{}: {}'.format(uri, size))
    return size

def worker(arg_dict):
    host = arg_dict['host']
    port = arg_dict['port']
    direction = arg_dict['dir']
    while True:
        size = get_size(host, port, direction)
        if size > 0:
            cmd = 'ls'
            if direction == 'ue2enb':
                cmd = 'ping -I oaitun_ue1 -c 1 -W 1 -s {} 10.0.1.1'.format(size)
            elif direction == 'enb2ue':
                cmd = 'ping -I oaitun_enb1 -c 1 -W 1 -s {} 10.0.1.2'.format(size)
            os.system(cmd)
        else:
            time.sleep(1)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('management_host')
    parser.add_argument('management_port')
    args = parser.parse_args()
    host = args.management_host
    port = args.management_port

    args_arr = [
        {
            "host": host,
            "port": port,
            "dir": "ue2enb"
        },{
            "host": host,
            "port": port,
            "dir": "enb2ue"
        }
    ]

    pool = Pool(2, init_worker)
    pool.map(worker, args_arr)