#!/bin/bash

host=$1
port=$2
oai=$3
log=$4
path=$(pwd)

# enb
cd $oai
source oaienv
cd cmake_targets/lte_build_oai/build
ENODEB=1 ./lte-softmodem -O $OPENAIR_HOME/ci-scripts/conf_files/lte-tdd-basic-sim.conf --basicsim --noS1 > $log/enb.log 2>&1 &
echo "start enb"

# ue
../../nas_sim_tools/build/conf2uedata -c $OPENAIR_HOME/openair3/NAS/TOOLS/ue_eurecom_test_sfr.conf -o .
./lte-uesoftmodem -C 2350000000 -r 25 --ue-rxgain 140 --basicsim --noS1 > $log/ue.log 2>&1 &
echo "start ue"