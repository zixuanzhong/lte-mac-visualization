#!/bin/bash

cd manager
./halt_ping.sh
sudo ./halt_oai.sh
cd ..
echo "[MANAGEMER] stops"

cd streamer
./halt.sh
cd ..
echo "[STREAMER] stops"

cd visualizer
./halt.sh
cd ..
echo "[VISUALIZER] stops"