#!/bin/bash

server=$1
log=$2

python3 mac_pdu_streamer.py $log/enb.log ${server} > $log/streamer.log 2>&1 &
echo "start log collector; server=$server"