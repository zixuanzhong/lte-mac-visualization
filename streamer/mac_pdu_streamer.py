import sys
import re
import os
import time
import graphyte
import argparse
import sys
import signal

def signal_handler(signal, frame):
    print("\nprogram exiting gracefully")
    sys.exit(0)

# [ZIXUAN-UL-MAC-PDU].1608015878401.UE_id:0.POWER_HEADROOM,1,3f;LONG_BSR,3,000000;SDU,424;
# [ZIXUAN-DL-MAC-PDU].1608135724786.UE_CONT_RES,6,5c944403a4f6;SDU,29;

lo =   [0,0,10,12,14,17,19,22,
        26,31,36,42,49,57,67,78,
        91,107,125,146,171,200,234,274,
        321,376,440,515,603,706,826,967,
        1132,1326,1552,1817,2127,2490,2915,3413,
        3995,4677,5476,6411,7505,8787,10287,12043,
        14099,16507,19325,22624,26487,31009,36304,42502,
        49759,58255,68201,79846,93479,109439,128125,150000]

hi =   [0,10,12,14,17,19,22,26,
        31,36,42,49,57,67,78,91,
        107,125,146,171,200,234,274,321,
        376,440,515,603,706,826,967,1132,
        1326,1552,1817,2127,2490,2915,3413,3995,
        4677,5476,6411,7505,8787,10287,12043,14099,
        16507,19325,22624,26487,31009,36304,42502,49759,
        58255,68201,79846,93479,109439,128125,150000,200000]

class MacPduAnalyzer:

    ul_sdu = {
        "ts": 0,
        "sdu": 0
    }

    ul_bsr = {
        "ts": 0,
        "bsr": 0
    }

    ul_phr_ph = {
        "ts": 0,
        "ph": 0
    }

    ul_crnti = {
        "ts": 0,
        "old_rnti": 0
    }

    dl_sdu = {
        "ts": 0,
        "sdu": 0
    }

    dl_drx = {
        "ts": 0,
        "cnt": 0
    }

    dl_timing_adv = {
        "ts": 0,
        "timing_adv": 0
    }

    dl_ue_content_res = {
        "ts": 0,
        "cnt": 0
    }

    pattern = re.compile("^.*\[ZIXUAN-(.*?)-MAC-PDU\]\.(.*?)$")
    def parse(self, line):
        matched = re.search(self.pattern, line.strip())
        if matched is not None:
            
            link = matched.group(1).strip()
            pdu = matched.group(2).split('.')
            # print(link)
            # print(pdu)
            # uplink
            if link == 'UL':
                sdu = 0
                bsr = 0
                ph = 0
                old_rnti = 0
                if len(pdu) != 3:
                    return
                try:
                    ts, ue_id, ces_sdus = int(pdu[0]), pdu[1].split(':')[1], pdu[2].split(';')
                    ts = int(ts/1000)
                except:
                    return
                for ce_sdu in ces_sdus:
                    try:
                        tlv = ce_sdu.split(',')
                        if len(tlv) == 2: #sdu
                            sdu_len = int(tlv[1])
                            sdu += sdu_len
                        elif len(tlv) == 3: #ce
                            t,l,v = tlv[0], int(tlv[1]), bytearray.fromhex(tlv[2])
                            if t == 'SHORT_BSR':
                                bsr = (v[0] & 0X3F)
                            if t == 'POWER_HEADROOM':
                                ph = (v[0] & 0X3F)
                            if t == 'CRNTI':
                                old_rnti = ((v[0]<<8) + v[1]) & (0XFFFF)
                    except:
                        print("UL exception, ts: {}".format(ts))
                        return
                bsr_lo = lo[bsr]
                bsr_hi = hi[bsr]
                if ts != self.ul_sdu['ts'] or sdu > self.ul_sdu['sdu']:
                    graphyte.send('mac.ul.sdu.size', sdu, timestamp=ts)
                    self.ul_sdu['ts'] = ts
                    self.ul_sdu['sdu'] = sdu
                if ts != self.ul_bsr['ts'] or bsr > self.ul_bsr['bsr']:
                    graphyte.send('mac.ul.ce.bsr.lo', bsr_lo, timestamp=ts)
                    graphyte.send('mac.ul.ce.bsr.hi', bsr_hi, timestamp=ts)
                    self.ul_bsr['ts'] = ts
                    self.ul_bsr['bsr'] = bsr
                if ts != self.ul_phr_ph['ts'] or ph > self.ul_phr_ph['ph']:
                    graphyte.send('mac.ul.ce.powerheadroom.ph', ph, timestamp=ts)
                    self.ul_phr_ph['ts'] = ts
                    self.ul_phr_ph['ph'] = ph
                if ts != self.ul_crnti['ts'] or old_rnti > self.ul_crnti['old_rnti']:
                    graphyte.send('mac.ul.ce.crnti.old_rnti', old_rnti, timestamp=ts)
                    self.ul_crnti['ts'] = ts
                    self.ul_crnti['old_rnti'] = old_rnti
                # print("ul_"+str(ts))
            if link == 'DL':
                # [ZIXUAN-DL-MAC-PDU].1608148088983.SDU,432;
                sdu = 0
                drx_cnt = 0
                timing_adv = 0
                ue_cont_res_cnt = 0
                if len(pdu) != 2:
                    return
                try:
                    ts, ces_sdus = int(pdu[0]), pdu[1].split(';')
                    ts = int(ts/1000)
                except:
                    return
                for ce_sdu in ces_sdus:
                    try:
                        tlv = ce_sdu.split(',')
                        if len(tlv) == 2: #sdu
                            sdu_len = int(tlv[1])
                            sdu += sdu_len
                        elif len(tlv) == 3: #ce
                            t,l,v = tlv[0], int(tlv[1]), bytearray.fromhex(tlv[2])
                            if t == 'DRX_CMD':
                                drx_cnt += 1
                            if t == 'TIMING_ADV_CMD':
                                timing_adv = (v[0] & 0X3F)
                            if t == 'UE_CONT_RES':
                                ue_cont_res_cnt += 1
                    except:
                        print("DL exception, ts: {}".format(ts))
                        return
                # sdu
                # print(sdu)
                if ts != self.dl_sdu['ts'] or sdu > self.dl_sdu['sdu']:
                    graphyte.send('mac.dl.sdu.size', sdu, timestamp=ts)
                    self.dl_sdu['ts'] = ts
                    self.dl_sdu['sdu'] = sdu
                # drx
                if ts == self.dl_drx['ts']:
                    self.dl_drx['cnt'] += 1
                elif ts != self.dl_drx['ts'] and (self.dl_drx['ts'] == 0 or self.dl_drx['cnt'] > 0):
                    graphyte.send('mac.dl.ce.drx.cnt', self.dl_drx['cnt'], timestamp=self.dl_drx['ts'])
                    self.dl_drx['ts'] = ts
                    self.dl_drx['cnt'] = drx_cnt
                # timing_adv
                if ts != self.dl_timing_adv['ts'] or timing_adv > self.dl_timing_adv['timing_adv']:
                    graphyte.send('mac.dl.ce.timing_adv', timing_adv, timestamp=ts)
                    self.dl_timing_adv['ts'] = ts
                    self.dl_timing_adv['timing_adv'] = timing_adv
                # ue_content_res
                if ts == self.dl_ue_content_res['ts']:
                    self.dl_ue_content_res['cnt'] += 1
                elif ts != self.dl_ue_content_res['ts'] and (self.dl_ue_content_res['ts'] == 0 or self.dl_ue_content_res['cnt'] > 0):
                    graphyte.send('mac.dl.ce.ue_content_res.cnt', self.dl_ue_content_res['cnt'], timestamp=self.dl_ue_content_res['ts'])
                    self.dl_ue_content_res['ts'] = ts
                    self.dl_ue_content_res['cnt'] = ue_cont_res_cnt
                # print("dl_"+str(ts))


if __name__ == "__main__":

    signal.signal(signal.SIGINT, signal_handler)

    parser = argparse.ArgumentParser()
    parser.add_argument('log_file')
    parser.add_argument('graphite_host')
    args = parser.parse_args()
    log_file = args.log_file
    graphite_host = args.graphite_host
    graphyte.init(graphite_host, prefix='oai')

    mac_pdu_analyzer = MacPduAnalyzer()
    # sender = graphyte.Sender('localhost', prefix='oai')

    try:
        with open(log_file) as f:
            while True:
                line = f.readline()
                if line:
                    mac_pdu_analyzer.parse(line)
                time.sleep(0.01)
    except KeyboardInterrupt:
        print('interrupted!')
    

