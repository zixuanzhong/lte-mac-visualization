#!/bin/bash

host=$1
port=$2
oai=$3
log=$4

cd visualizer
./launch.sh
cd ..
echo "[VISUALIZER] starts"

cd streamer
./launch.sh $host $log
cd ..
echo "[STREAMER] starts"

cd manager
sudo ./launch_oai.sh $host $port $oai $log
./launch_ping.sh $host $port $oai $log
cd ..
echo "[MANAGEMER] starts"